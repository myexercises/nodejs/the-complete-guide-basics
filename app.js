const http = require('http');
const router = require('./router');

const requestListener = (req, res) => {
    router(req, res);
};

const server = http.createServer(requestListener);

server.listen(3000);
