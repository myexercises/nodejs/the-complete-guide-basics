let userList = ['Ala', 'Kasia', 'Zosia'];

const renderListItem =
    output =>
        item =>
            output.write(`<li>${item}</li>`);

const router = (req, res) => {
    const renderUser = renderListItem(res);
    const { url, method} = req;
    res.setHeader('Content-Type', 'text/html');
    if (url === '/') {
        res.write('<html>');
        res.write('<body>');
        res.write(`<h1>Welcome to my page!</h1>`);
        res.write(`<p>Create new user:</p>`);
        res.write(`<form action="/create-user" method="POST">`);
        res.write(`<label for="username">Username: </label><input type="text" name="username" />`);
        res.write(`<button type="submit">Submit</button></form>`);
        res.write(`</form>`);
        res.write('</body>');
        res.write('</html>');
        res.end();
    }
    if (url === '/users') {
        res.write('<html>');
        res.write('<body>');
        res.write('<ol>');
        userList.map(renderUser);
        res.write('</ol>');
        res.write('</body>');
        res.write('</html>');
        res.end();
    }
    if (url === '/create-user' && method === 'POST') {
        const body = [];
        req.on('data', chunk => {
            body.push(chunk);
        });
        req.on('end', () => {
            const parsedBody = Buffer.concat(body).toString();
            const username = parsedBody.split('=')[1];
            userList = [...userList, username];
            res.write('<html>');
            res.write('<body>');
            res.write(`<p>User <strong>${username}</strong> created!</p>`);
            res.write('</body>');
            res.write('</html>');
            res.end();
        });
    }
}

module.exports = router;